package com.mmodpoww.apphomecafe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2

class DetailDrinkActivity : AppCompatActivity() {

    private lateinit var viewPager2: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val drink: Drink = intent.getParcelableExtra("Intent to Detail Activity")!!
        val drinkList: List<Drink> = Drink.drinkList(applicationContext)

        viewPager2 = findViewById(R.id.detail_view_pager)
        viewPager2.setPageTransformer(ZoomOutPageTransformer())

        val viewPagerAdapter = DetailDrinkViewPagerAdapter(drinkList, this, viewPager2)
        viewPager2.adapter = viewPagerAdapter
        viewPager2.setCurrentItem(drink.id, false)
    }
}