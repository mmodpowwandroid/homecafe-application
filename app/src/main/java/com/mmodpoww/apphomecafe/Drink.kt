package com.mmodpoww.apphomecafe
import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Drink(
    val id: Int,
    val banner: Int,
    val title: String,
    val subtitle: String,
    val about: String
) :
    Parcelable {

    companion object {
        fun drinkList(context: Context): List<Drink> {
            return listOf(
                Drink(
                    0, R.drawable.a,
                    context.getString(R.string.title_a),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_a)
                ),
                Drink(
                    1, R.drawable.b,
                    context.getString(R.string.title_b),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_b)
                ),
                Drink(
                    2, R.drawable.c,
                    context.getString(R.string.title_c),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_c)
                ),
                Drink(
                    3, R.drawable.d,
                    context.getString(R.string.title_d),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_d)
                ),
                Drink(
                    4, R.drawable.e,
                    context.getString(R.string.title_e),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_e)
                ),
                Drink(
                    5, R.drawable.f,
                    context.getString(R.string.title_f),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_f)
                ),
                Drink(
                    6, R.drawable.g,
                    context.getString(R.string.title_g),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_g)
                ),
                Drink(
                    7, R.drawable.h,
                    context.getString(R.string.title_h),
                    context.getString(R.string.subtitle_drink),
                    context.getString(R.string.about_h)
                )
            )
        }
    }
}