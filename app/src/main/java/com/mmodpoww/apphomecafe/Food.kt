package com.mmodpoww.apphomecafe
import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Food(
    val id: Int,
    val banner: Int,
    val title: String,
    val subtitle: String,
    val about: String
) :
    Parcelable {

    companion object {
        fun foodList(context: Context): List<Food> {
            return listOf(
                Food(
                    0, R.drawable.aa,
                    context.getString(R.string.title_aa),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_aa)
                ),
                Food(
                    1, R.drawable.bb,
                    context.getString(R.string.title_bb),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_bb)
                ),
                Food(
                    2, R.drawable.cc,
                    context.getString(R.string.title_cc),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_cc)
                ),
                Food(
                    3, R.drawable.dd,
                    context.getString(R.string.title_dd),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_dd)
                ),
                Food(
                    4, R.drawable.ee,
                    context.getString(R.string.title_ee),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_ee)
                ),
                Food(
                    5, R.drawable.ff,
                    context.getString(R.string.title_ff),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_ff)
                ),
                Food(
                    6, R.drawable.gg,
                    context.getString(R.string.title_gg),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_gg)
                ),
                Food(
                    7, R.drawable.hh,
                    context.getString(R.string.title_hh),
                    context.getString(R.string.subtitle_food),
                    context.getString(R.string.about_hh)
                )
            )
        }
    }
}