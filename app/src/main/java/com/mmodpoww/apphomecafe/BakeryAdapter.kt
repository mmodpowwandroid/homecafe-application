package com.mmodpoww.apphomecafe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class BakeryAdapter(
    private val bakeryList: List<Bakery>,
    private val onClickListener: OnClickListener
) :
    RecyclerView.Adapter<BakeryAdapter.BakeryViewHolder>() {

    class BakeryViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        private val banner: ImageView = itemView.findViewById(R.id.banner_item_image_view)
        private val title: TextView = itemView.findViewById(R.id.title_item_text_view)
        private val subtitle: TextView = itemView.findViewById(R.id.subtitle_item_text_view)

        fun bind(
            bakery: Bakery,
            onClickListener: OnClickListener
        ) {
            banner.setImageResource(bakery.banner)
            title.text = bakery.title
            subtitle.text = bakery.subtitle
            itemView.setOnClickListener {
                onClickListener.onClick(bakery)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BakeryViewHolder {
        return BakeryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_detail,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BakeryViewHolder, position: Int) {
        val bakery: Bakery = bakeryList[position]
        holder.bind(bakery, onClickListener)
    }

    override fun getItemCount() = bakeryList.size

    class OnClickListener(val clickListener: (bakery: Bakery) -> Unit) {
        fun onClick(bakery: Bakery) = clickListener(bakery)
    }
}