package com.mmodpoww.apphomecafe
import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Bakery(
    val id: Int,
    val banner: Int,
    val title: String,
    val subtitle: String,
    val about: String
) :
    Parcelable {

    companion object {
        fun bakeryList(context: Context): List<Bakery> {
            return listOf(
                Bakery(
                    0, R.drawable.cheescake,
                    context.getString(R.string.title_cheescake),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_ccnm)
                ),
                Bakery(
                    1, R.drawable.chocolate_lava,
                    context.getString(R.string.title_cclava),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_clv)
                ),
                Bakery(
                    2, R.drawable.cr_me_br_l_e,
                    context.getString(R.string.title_Crb),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_cbl)
                ),
                Bakery(
                    3, R.drawable.honey_toast,
                    context.getString(R.string.title_hnt),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_hnt)
                ),
                Bakery(
                    4, R.drawable.redvelvet,
                    context.getString(R.string.title_rvv),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_rvv)
                ),
                Bakery(
                    5, R.drawable.sandwich,
                    context.getString(R.string.title_sw),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_sw)
                ),
                Bakery(
                    6, R.drawable.tiramisu,
                    context.getString(R.string.title_tms),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_tms)
                ),
                Bakery(
                    5, R.drawable.toffee_cake,
                    context.getString(R.string.title_tfc),
                    context.getString(R.string.subtitle_bakery),
                    context.getString(R.string.about_tfc)
                )
            )
        }
    }
}