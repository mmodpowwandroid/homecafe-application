package com.mmodpoww.apphomecafe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class DrinkAdapter(
    private val drinkList: List<Drink>,
    private val onClickListener: OnClickListener
) :
    RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder>() {

    class DrinkViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        private val banner: ImageView = itemView.findViewById(R.id.banner_item_image_view)
        private val title: TextView = itemView.findViewById(R.id.title_item_text_view)
        private val subtitle: TextView = itemView.findViewById(R.id.subtitle_item_text_view)

        fun bind(
            drink : Drink,
            onClickListener: OnClickListener
        ) {
            banner.setImageResource(drink.banner)
            title.text = drink.title
            subtitle.text = drink.subtitle
            itemView.setOnClickListener {
                onClickListener.onClick(drink)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        return DrinkViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_detail,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        val drink : Drink = drinkList[position]
        holder.bind(drink, onClickListener)
    }

    override fun getItemCount() = drinkList.size

    class OnClickListener(val clickListener: (drink: Drink) -> Unit) {
        fun onClick(drink : Drink) = clickListener(drink)
    }
}