package com.mmodpoww.apphomecafe

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mmodpoww.apphomecafe.databinding.FragmentBakeryBinding
import com.mmodpoww.apphomecafe.databinding.FragmentDrinkBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DrinkFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DrinkFragment : Fragment() {
    private var _binding: FragmentDrinkBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    private val drinkItemListener = DrinkAdapter.OnClickListener { drink ->
        val intent = Intent(getActivity(), DetailDrinkActivity::class.java)
        intent.putExtra("Intent to Detail Activity", drink)
        getActivity()?.startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDrinkBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val drinkList: List<Drink> = Drink.drinkList(requireContext())
        val recyclerView: RecyclerView? = binding?.recyclerView
        val adapter = DrinkAdapter(drinkList,drinkItemListener)
        recyclerView?.adapter = adapter
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}