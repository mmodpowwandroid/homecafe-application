package com.mmodpoww.apphomecafe

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mmodpoww.apphomecafe.databinding.FragmentDrinkBinding
import com.mmodpoww.apphomecafe.databinding.FragmentFoodBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FoodFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FoodFragment : Fragment() {
    private var _binding: FragmentFoodBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    private val foodItemListener = FoodAdapter.OnClickListener { food ->
        val intent = Intent(getActivity(), DetailFoodActivity::class.java)
        intent.putExtra("Intent to Detail Activity", food)
        getActivity()?.startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFoodBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val foodList: List<Food> = Food.foodList(requireContext())
        val recyclerView: RecyclerView? = binding?.recyclerView
        val adapter = FoodAdapter(foodList,foodItemListener)
        recyclerView?.adapter = adapter
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}