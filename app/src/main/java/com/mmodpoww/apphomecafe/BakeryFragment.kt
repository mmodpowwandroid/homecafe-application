package com.mmodpoww.apphomecafe

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.mmodpoww.apphomecafe.databinding.FragmentBakeryBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BakeryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BakeryFragment : Fragment() {
    private var _binding: FragmentBakeryBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private val bakeryItemListener = BakeryAdapter.OnClickListener { bakery ->
        val intent = Intent(getActivity(), DetailActivity::class.java)
        intent.putExtra("Intent to Detail Activity", bakery)
        getActivity()?.startActivity(intent)
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBakeryBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bakeryList: List<Bakery> = Bakery.bakeryList(requireContext())
        val recyclerView: RecyclerView? = binding?.recyclerView
        val adapter = BakeryAdapter(bakeryList,bakeryItemListener)
        recyclerView?.adapter = adapter
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}